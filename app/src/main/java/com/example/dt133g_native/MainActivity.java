package com.example.dt133g_native;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void startBubbleTest(View view) {
        Button b = findViewById(view.getId());
        String text = b.getText().toString();
        String type;
        int val = Integer.parseInt(text.replaceAll("[^\\d.]", ""))*1000;
        type = text.contains("BEST") ? "BEST CASE" : "WORST CASE";
        BubblesortTest bubblesorttest = new BubblesortTest();
        bubblesorttest.runBubbletest(val, type);
    }

    public void startFibonacciTest(View view) {
        Button b = findViewById(view.getId());
        int val = Integer.parseInt(b.getText().toString().replaceAll("[^\\d.]", ""));
        FibonacciTest fibonacciTest = new FibonacciTest();
        fibonacciTest.runFibonacciTest(val);
    }

//        public void startMapsActivity(View view) {
//        startActivity(new Intent(getApplicationContext(), MapsActivity.class));
//    }
}
