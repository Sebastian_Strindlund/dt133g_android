package com.example.dt133g_native;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

class BubblesortTest {

    private String type;
    private int[] _testArray;

    private void initArray() {

        if (type.equals("BEST CASE")) {
            System.out.println("BEST CASE ARRAY of SIZE " +  _testArray.length);
            // Best Case array
            for (int i = 0; i < _testArray.length; i++) {
                _testArray[i] = i;
            }
        } else {
            System.out.println("WORST CASE ARRAY of SIZE " +  _testArray.length);
            // Worst Case Array
            int x = _testArray.length;
            for (int i = 0; i < _testArray.length; i++) {
                x--;
                _testArray[i] = x;
            }
        }
    }

    void runBubbletest(int val, String type) {
        _testArray = new int[val];
        this.type = type;
        initArray();

        int[] values;
        long start, stop, elapsed;

        values = _testArray.clone();
        start = System.nanoTime();
        for (int i = 0; i < values.length - 1; i++) {
            for (int j = 0; j < values.length - 1; j++) {
                if (values[j] > values[j + 1]) {
                    int temp = values[j];
                    values[j] = values[j+1];
                    values[j + 1] = temp;
                }
            }
        }
        stop = System.nanoTime();
        elapsed = TimeUnit.NANOSECONDS.toMillis(stop - start);
        System.out.println("** Native Version - Bubblesort with " + values.length/1000 + "K (" + type + ") time: " + elapsed + " ms **");

    }

//    void runBubbletest(int val, String type) {
//        _testArray = new int[val];
//        this.type = type;
//        initArray();
//
//        int[] values;
//        List<Long> result = new ArrayList<>();
//        long start;
//        long stop;
//        long elapsed;
//        long high = 0;
//        long low = 99999999;
//
//        // Run the test 10 times
//        for (int n = 0; n < 10; n++) {
//
//            values = _testArray.clone();
//            start = System.nanoTime();
//            for (int i = 0; i < values.length - 1; i++) {
//                for (int j = 0; j < values.length - 1; j++) {
//                    if (values[j] > values[j + 1]) {
//                        int temp = values[j];
//                        values[j] = values[j+1];
//                        values[j + 1] = temp;
//                    }
//                }
//            }
//            stop = System.nanoTime();
//            elapsed = TimeUnit.NANOSECONDS.toMillis(stop - start);
//
//            if (elapsed > high) {
//                high = elapsed;
//            }
//
//            if (elapsed < low) {
//                low = elapsed;
//            }
//            result.add(elapsed);
//            System.out.println("** Native Version - Bubblesort with " + values.length/1000 + "K (" + type + ") time: " + elapsed + " ms **");
//        }
//        long average = 0;
//        for(int i = 0; i < result.size(); i++) {
//            average += result.get(i);
//        }
//
//        average = average/result.size();
//        System.out.println("###########################################################");
//        System.out.println("High: " + high + " ms | Low: " + low + " ms | Average: " + average + " ms");
//        System.out.println("###########################################################");
//    }
}
