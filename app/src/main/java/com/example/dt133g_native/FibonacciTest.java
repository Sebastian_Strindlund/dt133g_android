package com.example.dt133g_native;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

class FibonacciTest {

    void runFibonacciTest(int val) {
        long start, stop, elapsed;
        start = System.nanoTime();
        Fibonacci(val);
        stop = System.nanoTime();
        elapsed = TimeUnit.NANOSECONDS.toMillis(stop - start);
        System.out.println("** Native version - Fibonacci(" + val + ") time elapsed: " + elapsed + " ms **");

    }

    private int Fibonacci(int val) {
        if (val < 2) {
            return val;
        }
        return Fibonacci(val - 2) + Fibonacci(val - 1);
    }

//    public void runFibonacciTest(int val) {
//        long start, stop, elapsed;
//        long high = 0;
//        long low = 999999999;
//        List<Long> result = new ArrayList<>();
//
//        for (int i = 0; i < 10; i++) {
//            start = System.nanoTime();
//            Fibonacci(val);
//            stop = System.nanoTime();
//            elapsed = TimeUnit.NANOSECONDS.toMillis(stop - start);
//            System.out.println("** Native version - Fibonacci(" + val + ") time elapsed: " + elapsed + " ms **");
//
//            if (elapsed > high) {
//                high = elapsed;
//            }
//
//            if (elapsed < low) {
//                low = elapsed;
//            }
//
//            result.add(elapsed);
//        }
//
//        long average = 0;
//        for(int i = 0; i < result.size(); i++) {
//            average += result.get(i);
//        }
//
//        average = average/result.size();
//        System.out.println("###########################################################");
//        System.out.println("High: " + high + " ms | Low: " + low + " ms | Average: " + average + " ms");
//        System.out.println("###########################################################");
//    }
}
